<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 11:13
 */

function createModel($class) {
    $dbConfig = config('database');
    
    return new $class($dbConfig);
}

function config($keyPath) {
    $path = explode('.', $keyPath);
    
    $configFile = 'config/' . array_shift($path) . '.php';
    
    if (!file_exists('./'.$configFile)) {
        throw new Exception("config file {$configFile} does not exists");
    }
    
    $config = require($configFile);
    
    foreach ($path as $key) {
        if (!array_key_exists($key, $config)) {
            throw new Exception("unknown config key {$keyPath}");
        }
        
        $config = $config[$key];
    }
    
    return $config;
}