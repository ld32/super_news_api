<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 10:46
 */
class NewsModel
{
    private $page;
    private $terms = [];
    private $orderBy;
    private $mapping = [
        'id' => 'news.id',
        'category' => 'categories.name',
        'created_at' => 'news.created_at'
    ];
    
    private $connection;
    
    public function __construct($dbConfig)
    {
        $dblocation = $dbConfig['location']; // Имя сервера
        $dbuser = $dbConfig['user'];         // Имя пользователя
        $dbpasswd = $dbConfig['password'];   // Пароль
        
        $this->connection = @mysql_connect($dblocation,$dbuser,$dbpasswd);

        if (!$this->connection) {
            throw new Exception("Database connection failed", 500);
        }

        if (!@mysql_select_db($dbConfig['database'], $this->connection))
        {
            throw new Exception("Cannot connect to database {$dbConfig['database']}", 500);
        }

    }

    public function __destruct () {
        if(!mysql_close($this->connection)) {
            throw new Exception("Cannot close connection to database", 500);
        }
    }

    public function page($page) {
        $this->page = $page;
        
        return $this;
    }

    public function category($category_id) {
        $this->terms[] = [
            'field' => 'categories.id',
            'equality' => '=',
            'value' => $category_id

        ];

        return $this;
    }

    public function from($from) {
        $this->terms[] = [
            'field' => 'news.created_at',
            'equality' => '>',
            'value' => $from

        ];

        return $this;
    }

    public function to($to) {
        $this->terms[] = [
            'field' => 'news.created_at',
            'equality' => '<',
            'value' => 'to'

        ];

        return $this;
    }

    public function orderBy($orderBy) {
        $this->orderBy = $orderBy;

        return $this;
    }

    public function get($fields = []) {
        $query = mysql_query($this->query($fields));

        $data = [];

        do {
            $item = mysql_fetch_array($query);

            if ($item == false) {
                break;
            }


            $data[] = $this->getItem($item, $fields);
        } while(true);

        $this->reset();

        return $data;
    }

    private function getItem($data, $fields) {
        $item = [];

        foreach ($fields as $field) {
            $item[$field] = $data[$field];
        }

        return $item;
    }

    protected function reset() {
        $this->page = null;
        $this->category_id = null;
        $this->from = null;
        $this->to = null;
    }

    protected function query($fields)  {
        $query = "select {$this->prepareFields($fields)} " .
                 "from news inner join categories " .
                 "on news.category_id = categories.id";

        if (!empty($this->terms)) {
            $query .= ' where ';

            $query .= implode(' and ', array_map(function ($term) {
                return implode(' ', $term);
            }, $this->terms));
        }

        if (!empty($this->orderBy)) {
            $query .= " order by {$this->orderBy}";
        }

        if (!empty($this->page)) {
            $count = config('database.items_on_page');
            $offset = $count * ($this->page - 1);

            $query .= " limit {$offset},{$count}";
        }

        $query .= ';';

        return $query;
    }

    protected function prepareFields($fields) {
        $select = [];

        foreach ($fields as $field) {
            if (array_key_exists($field ,$this->mapping)) {
                $select[] = $this->mapping[$field].' as '.$field;

                continue;
            }

            $select[] = $field;
        }

        if (empty($select)) {
            $select = ['*'];
        }

        return implode(', ', $select);
    }
}