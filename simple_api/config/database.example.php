<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 11:30
 */

return [
    'location' => 'localhost',
    'user' => 'root',
    'password' => '',
    'database' => 'super_news',
    'items_on_page' => 10
];