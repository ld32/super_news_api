<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 9:54
 */

return [
    '/news' => [
        'method' => 'GET',
        'controller' => 'NewsController',
        'function' => 'index'
    ]
];