<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 9:45
 */

//include ('./../models/');

class NewsController
{
    private $modelClass = NewsModel::class;
    private $model;
    
    public function __construct()
    {
        $this->model = createModel($this->modelClass);
    }

    public function index($params) {
        if (!empty($params['page'])) {
            $this->model->page($params['page']);
        }
        
        if (!empty($params['category_id'])) {
            $this->model->category($params['category_id']);
        }

        if (!empty($params['order_by'])) {
            $this->model->orderby($params['order_by']);
        } else {
            $this->model->orderby('created_at');
        }
        
        if (!empty($params['from'])) {
            $this->model->from($params['from']);
        }

        if (!empty($params['to'])) {
            $this->model->to($params['to']);
        }
        
        return [
            'code' => 200,
            'body' => $this->model->get([
                'id', 'title', 'body', 'created_at', 'category'
            ])
        ];
    }
}