<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 9:44
 */
//use Api\Controllers\NewsController;

include_once ('controllers/NewsController.php');
include_once ('models/NewsModel.php');
include_once ('helpers.php');

try {
    $routing = config('route');

    $route = $_SERVER['PATH_INFO'];
    $response = [
        'code' => 404,
        'content_type' => 'application/json',
        'body' => ''
    ];
    
    if (!empty($routing[$route])) {
        if ($routing[$route]['method'] != $_SERVER['REQUEST_METHOD']) {
            echo 'wrong method';
            return;
        }

        $controllerName = $routing[$route]['controller'];
        $method = $routing[$route]['function'];

        $controller = new $controllerName();

        $response = array_merge(
            $response,
            $controller->$method($_GET)
        );
    }
} catch (Exception $e) {
    $response['body'] = json_encode([
        'message' => $e->getMessage(),
        'stack_trace' => $e->getTraceAsString()
    ]);

    if (!empty($e->getCode())) {
        $response['code'] = $e->getCode();
    }
}

header('Content-Type: '.$response['content_type'].';charset=utf-8', true, $response['code']);

if ($response['content_type'] == 'application/json') {
    echo json_encode($response['body']);

    return;
}

echo $response['body'];
