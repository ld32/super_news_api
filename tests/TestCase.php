<?php

class TestCase extends Illuminate\Foundation\Testing\TestCase
{
    /**
     * The base URL to use while testing the application.
     *
     * @var string
     */
    protected $baseUrl = 'http://localhost';

    /**
     * Creates the application.
     *
     * @return \Illuminate\Foundation\Application
     */
    public function createApplication()
    {
        $app = require __DIR__.'/../bootstrap/app.php';

        $app->make(Illuminate\Contracts\Console\Kernel::class)->bootstrap();

        return $app;
    }
    
    public function getFixturePath($fn) {
        $class = str_replace('App\Tests\\', '', get_class($this));
        $class = str_replace('\\', '/', $class);

        return base_path('tests/fixtures') . "/{$class}/{$fn}";
    }

    public function getFixture($fn)
    {
        $data = file_get_contents($this->getFixturePath($fn));

        return $data;
    }

    public function getJsonFixture($fn, $assoc = false)
    {
        return json_decode($this->getFixture($fn), $assoc);
    }
}
