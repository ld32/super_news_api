<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 1:15
 */

namespace App\Services;


use App\Models\Image;
use Illuminate\Http\UploadedFile;

class ImageService
{
    public function check($image) {
        return preg_match('/^image/', $image->getMimeType());
    }

    public function addImage($id, $images) {
        /* @var $image UploadedFile */
        foreach ($images as $image) {
            if ($image->isValid() && $this->check($image)) {
                $extension = $image->getClientOriginalExtension();
                $md5 = md5($image->getClientOriginalName());

                $imageName = "{$id}_{$md5}.{$extension}";
                $imagePath = config('app.images.storage').'/'.$imageName;

                $image->move(
                    public_path(config('app.images.storage')),
                    $imageName
                );

                $this->updateOrCreate($id, $imagePath);
            }
        }
    }

    public function updateOrCreate($id, $path) {
        $query = Image::where('path', $path);

        if ($query->exists()) {
            $query->update([
                'news_id' => $id,
                'path' => $path
            ]);
        } else {
            Image::create([
                'news_id' => $id,
                'path' => $path
            ]);
        }
    }
}