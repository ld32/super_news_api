<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 16:12
 */

namespace App\Services;

use App\Models\Category;

class CategoriesService
{
    public function create($data) {
        Category::create($data);
    }
    
    public function item($id) {
        return Category::findOrFail($id);
    }
    
    public function delete($id) {
        Category::findOrFail($id)->delete();
    }
    
    public function update($id, $data) {
        Category::findOrFail($id)->update($data);
    }
}