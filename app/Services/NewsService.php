<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 16:11
 */

namespace App\Services;

use App\Models\News;

class NewsService
{
    public function create($data) {
        return News::create($data)->toArray();
    }
    
    public function item($id) {
        return News::findOrFail($id)->toArray();
    }

    public function delete($id) {
        News::findOrFail($id)->with('images')->delete();
    }
    
    public function get($attributes) {
        $query = News::query();
        
        if (!empty($attributes['category_id'])) {
            $query->where('category_id', $attributes['category_id']);
        }

        if (!empty($attributes['from'])) {
            $query->where('created_at', '>', $attributes['from']);
        }
        
        if (!empty($attributes['to'])) {
            $query->where('created_at', '<', $attributes['to']);
        }
        
        return $query->with('images')->paginate()->toArray()['data'];
    }

    public function search($query) {
        return News::whereRaw("title like \"%{$query}%\"")
            ->orWhereRaw("body like \"%{$query}%\"")
            ->with('images')
            ->get()->toArray();
    }
    
    public function update($id, $data) {
        News::findOrFail($id)->update($data);
    }

    public function exists($id) {
        return News::where('id', $id)->exists();
    }
}