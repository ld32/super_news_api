<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('welcome');
});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/

Route::group(['middleware' => ['web', 'api']], function () {
    Route::get('/news', ['as' => 'news.list', 'uses' => 'NewsController@index']);
    Route::get('/news/search', ['as' => 'news.search', 'uses' => 'NewsController@search']);
    Route::get('/news/{id}', ['as' => 'news.view', 'uses' => 'NewsController@view']);
    Route::put('/news/{id}', ['as' => 'news.update', 'uses' => 'NewsController@update']);
    Route::post('/news', ['as' => 'news.create', 'uses' => 'NewsController@create']);
    Route::post('/news/{id}/image', ['as' => 'news.image', 'uses' => 'NewsController@addImage']);
    Route::delete('/news/{id}', ['as' => 'news.delete', 'uses' => 'NewsController@delete']);

    Route::get('/categories', ['as' => 'categories.list', 'uses' => 'CategoryController@index']);
    Route::put('/categories/{id}', ['as' => 'categories.update', 'uses' => 'CategoryController@update']);
    Route::post('/categories', ['as' => 'categories.create', 'uses' => 'CategoryController@create']);
    Route::delete('/categories/{id}', ['as' => 'categories.delete', 'uses' => 'CategoryController@delete']);
});