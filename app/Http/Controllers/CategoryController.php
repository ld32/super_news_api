<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 0:22
 */

namespace App\Http\Controllers;


use App\Http\Requests\ChangeCategoryRequest;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\DeleteCategoryRequest;
use Symfony\Component\HttpFoundation\Response;
use App\Services\CategoriesService;

class CategoryController extends Controller
{
    public function update(ChangeCategoryRequest $request, CategoriesService $categoriesService, $id) {
        $categoriesService->update($id, $request->all());
        
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function create(CreateCategoryRequest $request, CategoriesService $categoriesService) {
        $categoriesService->create($request->all());
        
        return response('', Response::HTTP_NO_CONTENT);
    }
    
    public function delete(DeleteCategoryRequest $request, CategoriesService $categoriesService, $id) {
        $categoriesService->delete($id);
        
        return response('', Response::HTTP_NO_CONTENT);
    }
}