<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 0:22
 */

namespace App\Http\Controllers;


use App\Http\Requests\AddImageRequest;
use App\Http\Requests\AddNewsRequest;
use App\Http\Requests\ChangeNewsRequest;
use App\Http\Requests\DeleteNewsRequest;
use App\Http\Requests\FindNewsRequest;
use App\Http\Requests\GetNewsRequest;
use App\Services\ImageService;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;
use App\Services\NewsService;
class NewsController extends Controller
{
    public function index(GetNewsRequest $request, NewsService $newsService) {        
        $items = $newsService->get(array_except($request->all(), 'page'));

        return app(ResponseFactory::class)->json([
            'news' => $items
        ], Response::HTTP_OK);
    }

    public function view(NewsService $newsService, $id) {
        try {
            return response()->json(
                ['news' => $newsService->item($id)],
                Response::HTTP_OK
            );
        } catch (\Exception $e) {
            return response()->json(
                ['message' => 'news does not exists'],
                Response::HTTP_NOT_FOUND
            );
        }
    }

    public function search(FindNewsRequest $request, NewsService $newsService) {
        $query = $newsService->search($request->input('query'));
        
        return response()->json(
            ['news' => $query],
            Response::HTTP_OK
        );
    }

    public function update(ChangeNewsRequest $request, NewsService $newsService, $id) {
        $newsService->update($id, $request->all());
        
        return response('', Response::HTTP_NO_CONTENT);
    }

    public function create(AddNewsRequest $request, NewsService $newsService) {
        $news = [
            'category_id' => $request->input('category_id'),
            'title' => $request->input('title'),
            'body' => $request->input('body')
        ];

        $createdNews = $newsService->create($news);

        if (!empty($request->file())) {
            $newsService->addImage($createdNews['id'], $request->file());
        }

        return response('', Response::HTTP_NO_CONTENT);
    }

    public function delete(DeleteNewsRequest $request, NewsService $newsService, $id) {
        $newsService->delete($id);
        
        return response('', Response::HTTP_NO_CONTENT);
    }
    
    public function addImage(AddImageRequest $request,
                             NewsService $newsService,
                             ImageService $imageService,
                             $id) {
        if (!$newsService->exists($id)) {
            return response()->json(['message' => 'News not found'], Response::HTTP_NOT_FOUND); 
        }
        
        $imageService->addImage($id, $request->file());

        return response()->json(
            ['news' => $newsService->item($id)],
            Response::HTTP_OK
        );
    } 
}