<?php
/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 29.02.16
 * Time: 0:09
 */

namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class Image extends Model
{
    protected $fillable = ['id', 'path', 'news_id'];
    
    public function news() {
        return $this->belongsTo(Category::class);
    }
}