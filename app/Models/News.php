<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 8:40
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class News extends Model
{
    protected $fillable = ['id', 'category_id', 'title', 'body', 'image'];

    public function category() {
        return $this->belongsTo(Category::class);
    }

    public function images()
    {
        return $this->hasMany(Image::class);
    }
}