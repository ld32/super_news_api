<?php

/**
 * Created by PhpStorm.
 * User: ascet
 * Date: 27.02.16
 * Time: 8:38
 */

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable = ['id', 'name'];

    public function news()
    {
        return $this->hasMany(News::class);
    }
}